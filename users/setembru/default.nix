{
  users.users.setembru = {
    isNormalUser = true;
    extraGroups = [
      "networkmanager"
    ];
    initialHashedPassword = "$y$j9T$xLjyGIL9uXlnEvmrf8kJa/$82VTere29qcOesmuTFVbw0efNVpK0xMc9a30gCjW9l8";
  };
}
