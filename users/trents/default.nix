{
  users.users.trents = {
    isNormalUser = true;
    extraGroups = [
      "networkmanager"
    ];
    initialHashedPassword = "$6$2EcxpAdz$RBdfvHZSOFQ8La5cjH2IrYGWqHamx8yWTZfRUPRM/nYe/JWNIlwFzTyr09MMqNDitN.8eiPjM/XieiSWxld951";
  };
}
